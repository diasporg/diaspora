#!/usr/bin/perl -T

use Data::Validate::IP qw(is_ipv4 is_ipv6);
use strict;
use warnings;

## Customized for diasp.org setup
## Modify: app/controllers/registrations_controller.rb:      
## With: logger.info "event=registration status=successful user=#{@user.diaspora_handle} ip=#{request.remote_ip}"
## assumes a manual fail2ban jail called manban created

  my $log = "/home/david/diaspora/log/production.log";

## how many seconds before an unseen ip is considered old and removed from the hash?
  my $expire_time = 9200;

## how many error log lines before we trigger blocking abusive ips and clean up
## of old ips in the hash? make sure this value is greater than $errors_block above.
  my $cleanup_time = 10;

## clear the environment and set our path
  $ENV{ENV} ="";
  $ENV{PATH} = "/bin:/usr/bin:/usr/local/bin";

## declare some internal variables and the hash of abusive ip addresses
  my ( $ip, $errors, $time, $newtime, $newerrors, $username, $date, $existingips, $isbl, $email );
  my $trigger_count=1;
  my %abusive_ips = ();

## open the log file. we are using the system binary tail which is smart enough
## to follow rotating logs. We could have used File::Tail, but tail is easier.
  open(LOG,"/usr/bin/tail -8000 $log |") || die "ERROR: could not open log file.\n";

  while(<LOG>) {
       ## process the log line if it contains one of these error codes 
       if ($_ =~ m/"\/users\/sign_up/)
         {

         ## Whitelisted ips. This is where you can whitelist ips that cause errors,
         if ($_ !~ m/127\.0\.0\.1/)
         {

         ## extract the ip address from the log line and get the current unix time
          $time = time();
          #($ip) = $_ =~ m/ip=(.*?)\s/;
          ($ip) = $_ =~ m/for (.*?)\s/;
          #($username) = $_ =~ m/user=(.*?)\s/;
          #($email) = $_ =~ m/email=(.*?)\s/;

         ## if an ip address has never been seen before we need
         ## to initialize the errors value to avoid warning messages.
          $abusive_ips{ $ip }{ 'errors' } = 0 if not defined $abusive_ips{ $ip }{ 'errors' };

         ## increment the error counter and update the time stamp.
          $abusive_ips{ $ip }{ 'errors' } = $abusive_ips{ $ip }->{ 'errors' } + 1;
          $abusive_ips{ $ip }{ 'time' } = $time;

           if ($abusive_ips{ $ip }->{ 'errors' } >= 0 ) {
              $existingips = `fail2ban-client get manban banip`;
              if (index($existingips, $ip) != -1) {
                ## already banned this ip
              } else {
		if (is_ipv4($ip)) {
                $isbl = `/home/david/diaspora/script/dnsbl-check.sh -s $ip`;
                if ($isbl > 0) {
                  print "bad IP from DNSBL detected from IP=$ip to sign_up, triggered banip on $ip/16 \n https://multirbl.valli.org/lookup/$ip.html \n ";
                  system("fail2ban-client set manban banip $ip;fail2ban-client get manban banip --with-time");
                } else {
                  #print "good IP from DNSBL detected from IP=$ip to sign_up \n https://multirbl.valli.org/lookup/$ip.html \n ";
                }
              }
		}
           }

         ## increment the trigger counter which is used for the following clean up function. 
          $trigger_count++;

         ## clean up function: when the trigger counter reaches the $cleanup_time we
         ## remove any old hash entries from the $abusive_ips hash
          if ($trigger_count >= $cleanup_time) {
             my $time_current =  time();

              ## clean up ip addresses we have not seen in a long time
               while (($ip, $time) = each(%abusive_ips)){

                  if ( ($time_current - $abusive_ips{ $ip }->{ 'time' } ) >= $expire_time) {
                       delete($abusive_ips{ $ip });
                  }
               }

             ## reset the trigger counter
              $trigger_count = 1;
          }
         }
       }
  }
#### EOF ####
