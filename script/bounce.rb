
# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'
user = User.find_by(email: ARGV[0])
user.disable_mail = true
user.save
admin = User.find_by_username(AppConfig.admins.account.get)
msg = "#{ARGV[0]} email disabled. signins:#{user.sign_in_count}  getting started:#{user.getting_started} Handle:#{user.username}@diasp.org IP: #{user.last_sign_in_ip} created:#{user.created_at} language:#{user.language} last_sign_in_at:#{user.last_sign_in_at}  "
mail = Notifier.single_admin(msg, admin, :subject => "Email diasbled #{ARGV[0]}").deliver_now
