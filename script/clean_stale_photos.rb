#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'


User.where(subscriber_donator: false, locked_at: [nil, ""], last_sign_in_at: 20.years.ago..5.years.ago).find_each do |user|
person = Person.find_by(diaspora_handle: user.username + '@diasp.org')
if person 
    Photo.where(author_id: person.id).each do |photo|

print photo.unprocessed_image
print " "
print person.diaspora_handle
photo.destroy
print "\n"
    end

end
end
