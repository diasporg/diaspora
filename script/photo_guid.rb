#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'
guid = ARGV[0].scan(/(.*?_.*?)_(.*?)\./)
user = Photo.find_by(random_string: guid.map(&:last))
person = Person.find_by(id: user.author_id)
user = User.find_by(email: ARGV[1])
msg = "Match found for #{ARGV[0]} they are #{person.diaspora_handle} https://diasp.org/people/#{person.guid}"
mail = Notifier.single_admin(msg.html_safe, user, :subject => "GUID Match for #{ARGV[0]}").deliver_now
