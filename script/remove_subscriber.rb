
# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'
user = User.find_by(username: ARGV[0])
user.subscriber_donator = false
user.save
user = User.find_by(username: ARGV[1])
msg = "username #{ARGV[0]} set to non subscriber"
mail = Notifier.single_admin(msg.html_safe, user, :subject => "username #{ARGV[0]} subscriber removed").deliver_now
