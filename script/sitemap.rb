require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'https://diasp.org'
SitemapGenerator::Sitemap.create do
  add_to_index '/public.xml', :changefreq => 'hourly', :priority => 0.9
  add '/public', :changefreq => 'hourly', :priority => 0.9
  add '/statistics', :changefreq => 'weekly'
end
SitemapGenerator::Sitemap.ping_search_engines # Not needed if you use the rake tasks
