#/bin/sh
(
  echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">"
  psql diaspora_production -c "COPY (select xmlelement(name url, xmlforest('https://diasp.org/posts/' || id as loc,to_char(updated_at, 'YYYY-MM-DDThh:mm:ss+07:00') AS lastmod), xmlelement(name changefreq, 'hourly')) from posts where public order by created_at DESC limit 40000) TO STDOUT" 
  echo "</urlset>"
) > /home/david/diaspora/public/public.xml
