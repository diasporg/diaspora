
# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'
user = User.find_by(username: ARGV[0])
user.subscriber_donator = true
user.remove_after = ''
user.subscriber_last_date = ARGV[1]
user.subscriber_notes = ARGV[2]
user.subscriber_recurring  = ARGV[3]
user.subscriber_donorbox = ARGV[4]
user.save
userwho = Person.find_by(diaspora_handle: ARGV[0] + '@diasp.org')
#puts userwho.guid
msg = "username #{ARGV[0]} set to subscriber. GUID #{userwho.guid}"
admin = User.find_by_username(AppConfig.admins.account.get)
mail = Notifier.single_admin(msg.html_safe, admin, :subject => "username #{ARGV[0]} subscriber added").deliver_now


