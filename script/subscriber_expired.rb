#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'

#select username, email, subscriber_notes, subscriber_last_date, subscriber_recurring from users where subscriber_last_date = current_date - interval '1 year' and subscriber_recurring = false;
User.where(subscriber_recurring: false, subscriber_donator: true, subscriber_last_date: 366.days.ago.beginning_of_day..366.days.ago.end_of_day).find_each do |user|
  msg = 'donator is a one time that should now be set to subscriber_donator:false in the db'
  puts user.username
user.subscriber_donator = false
#user.subscriber_last_date = ''
user.subscriber_notes = 'was a one time donator over 1 year ago'
user.save

  admin = User.find_by_username(AppConfig.admins.account.get)
  mail = Notifier.single_admin(msg.html_safe, admin, :subject => "username #{user.username} subscriber one time over a year, flag removed for donator").deliver_now
end
