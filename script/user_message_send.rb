
# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'
person = Person.find_by(diaspora_handle: ARGV[0])

    sender_username = AppConfig.admins.account.get
    sender = User.find_by(username: sender_username)
    conversation = sender.build_conversation(
      participant_ids: [sender.person.id, person.id],
      subject:         'Testing123',
      message:         {text: 'test message to you from administrator account, round 2 try'}
    )

    Diaspora::Federation::Dispatcher.build(sender, conversation).dispatch if conversation.save
