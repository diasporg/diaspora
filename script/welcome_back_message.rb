#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'


User.where(current_sign_in_at: 1.days.ago.beginning_of_day..1.days.ago.end_of_day, subscriber_donator: false, locked_at: [nil, ""], last_sign_in_at: 9.years.ago..100.days.ago).find_each do |user|
person = Person.find_by(diaspora_handle: user.username + '@diasp.org')


    sender_username = AppConfig.admins.account.get
    sender = User.find_by(username: sender_username)
    conversation = sender.build_conversation(
      participant_ids: [sender.person.id, person.id],
      subject:         '2022? Already?',
      message:         {text: 'Welcome back! Just the normal quick and annoying note to help support your pod for 2022! https://diasp.org/supportyourpod'}
    )

    Diaspora::Federation::Dispatcher.build(sender, conversation).dispatch if conversation.save

print user.username
print user.last_sign_in_at
print user.current_sign_in_at
print " "
end
