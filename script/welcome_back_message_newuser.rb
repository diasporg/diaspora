#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'


User.where(created_at: 30.days.ago.beginning_of_day..30.days.ago.end_of_day, subscriber_donator: false, locked_at: [nil, ""]).find_each do |user|
#created_at: 30.days.ago.beginning_of_day..30.days.ago.end_of_day
person = Person.find_by(diaspora_handle: user.username + '@diasp.org')


    sender_username = AppConfig.admins.account.get
    sender = User.find_by(username: sender_username)
    conversation = sender.build_conversation(
      participant_ids: [sender.person.id, person.id],
      subject:         'Happy 1 month diasp.org anniversary!',
      message:         {text: 'Hope you have had some time to explore the last month. This is just the normal quick and annoying note to help support your pod as mentioned in your email invite and when you joined up! https://diasp.org/supportyourpod Thank you ahead of time for your support! This only happens with your help.'}
    )

    Diaspora::Federation::Dispatcher.build(sender, conversation).dispatch if conversation.save

print user.username
print user.last_sign_in_at
print user.current_sign_in_at
print " "
end
