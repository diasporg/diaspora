#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'


User.where(current_sign_in_at: 1.days.ago.beginning_of_day..1.days.ago.end_of_day, subscriber_donator: false, locked_at: [nil, ""], last_sign_in_at: 9.years.ago..200.days.ago).find_each do |user|
person = Person.find_by(diaspora_handle: user.username + '@diasp.org')


    sender_username = AppConfig.admins.account.get
    sender = User.find_by(username: sender_username)
    conversation = sender.build_conversation(
      participant_ids: [sender.person.id, person.id],
      subject:         'Well hello again!',
      message:         {text: 'Welcome back, have not seen you around in a long time! Just the normal quick and annoying note to help support your pod! https://diasp.org/supportyourpod Thank you ahead of time for your support! This only happens with your help.'}
    )

    Diaspora::Federation::Dispatcher.build(sender, conversation).dispatch if conversation.save

print user.username
print user.last_sign_in_at
print user.current_sign_in_at
print " "
end
