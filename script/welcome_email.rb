#!/home/david/.rvm/rubies/ruby-3.3.3/bin/ruby

# Load diaspora environment
ENV['RAILS_ENV'] ||= "production" 
ENV['DB'] ||= "postgres"
require_relative '../config/environment'

User.where(created_at: 30.days.ago.beginning_of_day..30.days.ago.end_of_day, subscriber_donator: false, locked_at: [nil, ""]).find_each do |user|
  msg = 'I hope you are doing well and enjoying diasp.org<br><br> I sent you a note about a month ago when you signed up in the site and wanted to followup with you directly. '\
        'As you know diasp.org has no ads and does not sell your data, to sustain this model, I do have to ask users for donations, not something I love but I am commited to not throwing up banner ads all over.<br>'\
        '<br>User donations go directly to pay the server costs and other costs for hosting this platform. Even a small donation subscription makes it possible to reach the goals needed each year.<br>'\
        '<br>You can respond to me directly here, this is from my personal address. If you need other options like crypto or paypal or anything, just ask, we can figure something out.<br><br>'\
        '<b>Thank you ahead of time for your support</b>, here is that link for setting up a plan: '
  Notifier.welcome(msg.html_safe, user, :subject => "Note from diasp.org owner to #{user.username}").deliver_now
end
